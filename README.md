# Redmine "farend bleuclair" adapted for XiVO


## Installation

- Redmine3.4
```bash
$ git clone -b master https://gitlab.com/lmeiller/redmine_theme_farend_bleuclair.git /var/srv/redmine/redmine/public/themes/bleuclair
```


## Development

* Prerequisites : docker, docker-compose, node 13

```bash
$ cd /your/path/redmine/public/themes/bleuclair/src
$ npm i
$ docker-compose up
```

Once node server is running, each time you save .scss file, a new production css stylesheet is generated.

## LICENSE

GNU GPL v2  
https://gitlab.com/lmeiller/redmine_theme_farend_bleuclair/blob/master/LICENSE

## Credits

https://github.com/farend/redmine_theme_farend_bleuclair/

[ファーエンドテクノロジー株式会社](https://www.farend.co.jp/)
